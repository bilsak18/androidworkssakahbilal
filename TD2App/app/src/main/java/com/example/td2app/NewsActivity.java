package com.example.td2app;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

public class NewsActivity extends AppCompatActivity implements View.OnClickListener {

    private String login;

    Button logout, details;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.login = this.getIntent().getStringExtra("login");
        setContentView(R.layout.activity_news);

        logout = (Button) findViewById(R.id.logout);
        details = (Button) findViewById(R.id.details);

        logout.setOnClickListener(this);
        details.setOnClickListener(this);
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    @Override
    public void onClick(View view) {

        Intent logoutIntent = new Intent(this, LoginActivity.class);
        Intent detailsIntent = new Intent(this, DetailsActivity.class);
        detailsIntent.putExtra("login", this.getLogin());

        switch (view.getId()) {
            case R.id.logout:
                startActivity(logoutIntent);
                break;

            case R.id.details:
                startActivity(detailsIntent);
                break;

            default:
        }
    }
}
