package com.example.td3app;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class MyVideoGamesAdapter extends RecyclerView.Adapter<MyVideoGamesHolder> {

    private List<JeuVideo> gamesList;

    public MyVideoGamesAdapter(List<JeuVideo> gamesList) {
        this.gamesList = gamesList;
    }

    @NonNull
    @Override
    public MyVideoGamesHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_jeu_video,parent,false);
        return new MyVideoGamesHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyVideoGamesHolder holder, int position) {
        holder.display(gamesList.get(position));
    }

    @Override
    public int getItemCount() {
        return gamesList.size();
    }
}
