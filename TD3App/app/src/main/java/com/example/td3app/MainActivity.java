package com.example.td3app;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class MainActivity extends AppCompatActivity {

    List<JeuVideo> gamesList = new ArrayList<>();
    RecyclerView myRecyclerViews;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        String[] gamesNamesList = {"PUBG", "FIFA 20", "Assassin's Creed Brotherhood", "Call Of Duty", "Need For Speed World", "PES 20", "Assassin's Creed Revelations", "Fortnite", "Asphalt", "Red Dead Redemption", "League Of Legends", "Dofus"};
        for(int i=0;i<gamesNamesList.length;i++) {
            gamesList.add(new JeuVideo(gamesNamesList[i], new Random().nextInt(71)+30));
        }
        MyVideoGamesAdapter adapter = new MyVideoGamesAdapter(gamesList);

        myRecyclerViews = (RecyclerView) findViewById(R.id.myRecyclerViews);
        myRecyclerViews.setLayoutManager(new LinearLayoutManager(this));
        myRecyclerViews.setAdapter(adapter);
    }
}
