package com.example.td3app;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class MyVideoGamesHolder extends RecyclerView.ViewHolder {

    private TextView name, price;

    public MyVideoGamesHolder(@NonNull View itemView) {
        super(itemView);

        name = itemView.findViewById(R.id.name);
        price = itemView.findViewById(R.id.price);
    }

    void display(JeuVideo jeuVideo) {
        name.setText(jeuVideo.getName());
        price.setText(jeuVideo.getPrice() + " $");
    }
}
