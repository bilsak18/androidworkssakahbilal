package com.example.td4app;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class RepoAdapter extends RecyclerView.Adapter<RepoHolder> {

    private List<Repo> listRepos;

    public RepoAdapter(List<Repo> listRepos) {
        this.listRepos = listRepos;
    }

    @NonNull
    @Override
    public RepoHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_repo,parent,false);
        return new RepoHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RepoHolder holder, int position) {
        holder.display(listRepos.get(position));
    }

    @Override
    public int getItemCount() {
        return listRepos.size();
    }
}
