package com.example.td4app;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class RepoHolder extends RecyclerView.ViewHolder {

    private TextView repoName, userLogin, userId;

    public RepoHolder(@NonNull View itemView) {
        super(itemView);

        repoName = (TextView) itemView.findViewById(R.id.repoName);
        userLogin = (TextView) itemView.findViewById(R.id.userLogin);
        userId = (TextView) itemView.findViewById(R.id.userId);
    }

    void display(Repo repo) {
        repoName.setText(repo.getName());
        userId.setText(Integer.toString(repo.getOwner().getId()));
        userLogin.setText(repo.getOwner().getLogin());
    }
}
