package com.example.td1app;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    Button number1, number2, number3, number4, number5, number6, number7, number8, number9, number0;
    TextView screen, reset;
    Button bEquals;

    Button add, sub, mul, div;

    double result = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        number1 = (Button) findViewById(R.id.number1);
        number2 = (Button) findViewById(R.id.number2);
        number3 = (Button) findViewById(R.id.number3);
        number4 = (Button) findViewById(R.id.number4);
        number5 = (Button) findViewById(R.id.number5);
        number6 = (Button) findViewById(R.id.number6);
        number7 = (Button) findViewById(R.id.number7);
        number8 = (Button) findViewById(R.id.number8);
        number9 = (Button) findViewById(R.id.number9);
        number0 = (Button) findViewById(R.id.number0);
        screen = (TextView) findViewById(R.id.screen);
        bEquals = (Button) findViewById(R.id.equals);
        reset = (Button) findViewById(R.id.reset);

        add = (Button) findViewById(R.id.add);
        sub = (Button) findViewById(R.id.sub);
        mul = (Button) findViewById(R.id.mul);
        div = (Button) findViewById(R.id.div);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        /*if(screen.getText().length() != 0) {
            if(screen.getText().charAt(screen.getText().length()-1) == '+' || screen.getText().charAt(screen.getText().length()-1) == '-' || screen.getText().charAt(screen.getText().length()-1) == '×' || screen.getText().charAt(screen.getText().length()-1) == '÷') {
                bEquals.setClickable(false);
            } else {
                bEquals.setClickable(true);
            }
        }*/

        number1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                screen.setText(screen.getText() + "1");
            }
        });

        number2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                screen.setText(screen.getText() + "2");
            }
        });

        number3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                screen.setText(screen.getText() + "3");
            }
        });

        number4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                screen.setText(screen.getText() + "4");
            }
        });

        number5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                screen.setText(screen.getText() + "5");
            }
        });

        number6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                screen.setText(screen.getText() + "6");
            }
        });

        number7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                screen.setText(screen.getText() + "7");
            }
        });

        number8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                screen.setText(screen.getText() + "8");
            }
        });

        number9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                screen.setText(screen.getText() + "9");
            }
        });

        number0.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(screen.getText().length() != 0)
                    screen.setText(screen.getText() + "0");
            }
        });

        reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                screen.setText("");
            }
        });

        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(screen.getText().length() != 0) {
                    screen.setText(screen.getText() + "+");
                }
            }
        });

        sub.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(screen.getText().length() != 0) {
                    screen.setText(screen.getText() + "-");
                }
            }
        });

        mul.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(screen.getText().length() != 0) {
                    screen.setText(screen.getText() + "×");
                }
            }
        });

        div.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(screen.getText().length() != 0) {
                    screen.setText(screen.getText() + "÷");
                }
            }
        });

        bEquals.setOnClickListener(new View.OnClickListener(

        ) {
            @Override
            public void onClick(View view) {

            }
        });
    }

    /*public void computeAdd(View v) {
        int result = Integer.parseInt(etNumber1.getText().toString()) + Integer.parseInt(etNumber2.getText().toString());
        tvResult.setText(Integer.toString(result));
    }

    public void computeSub(View v) {
        int result = Integer.parseInt(etNumber1.getText().toString()) - Integer.parseInt(etNumber2.getText().toString());
        tvResult.setText(Integer.toString(result));
    }

    public void computeMul(View v) {
        int result = Integer.parseInt(etNumber1.getText().toString()) * Integer.parseInt(etNumber2.getText().toString());
        tvResult.setText(Integer.toString(result));
    }

    public void computeDiv(View v) {
        try {
            double result = Double.parseDouble(etNumber1.getText().toString()) / Double.parseDouble(etNumber2.getText().toString());
            tvResult.setText(Double.toString(result));
        } catch(ArithmeticException e) {
            Snackbar.make(v, "You should not divide by 0", Snackbar.LENGTH_LONG).show();
        }
    }*/

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
